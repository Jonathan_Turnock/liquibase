FROM openjdk:8-jre

ENV LIQUIBASE_RELEASE 3.6.3
ENV USERNAME liquibase
ENV PASSWORD liquibase
ENV DATABASE postgres
ENV HOSTNAME edw-postgres
ENV PORT 5432
ENV ACTION migrate

RUN apt-get install wget && mkdir -p /var/opt/liquibase && \
        cd /var/opt/liquibase && \
        wget https://github.com/liquibase/liquibase/releases/download/liquibase-parent-$LIQUIBASE_RELEASE/liquibase-$LIQUIBASE_RELEASE-bin.zip && \
        unzip liquibase-$LIQUIBASE_RELEASE-bin.zip && rm liquibase-$LIQUIBASE_RELEASE-bin.zip && \
        cd lib && wget https://jdbc.postgresql.org/download/postgresql-42.2.6.jar && \
        ln -s /var/opt/liquibase/liquibase /usr/local/bin/liquibase

WORKDIR /home

COPY changelog /home

ENTRYPOINT liquibase --driver=org.postgresql.Driver --changeLogFile=db.changelog.master.yml --url=jdbc:postgresql://$HOSTNAME:$PORT/$DATABASE --username=$USERNAME --password=$PASSWORD $ACTION