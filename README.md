
# Liquibase
The liquibase project contains the schema definition for the FX Quants persistance layer.
All modifications should go through liquibase and will be run in by a seperate docker container to manage database migrations.
This docker container can be used to create a fresh instance of the whole database or to patch an existing database through the use of changesets.

## Installation

Simply run the docker container alongside your current database installation and it will patch itself accordingly. To run in a specific version of the database run the docker container version assocaited
with that release.

## Usage
For example to run the latest database scripts run the following:
```
docker run --rm -e USERNAME=liquibase -e PASSWORD=liquibase -e HOSTNAME=postgres -e fxquants/edw-liquibase
```
The following environment variables can be used to change the behaviour:   

| Env Var  | Usage                                                    | Default                                     |
|----------|----------------------------------------------------------|---------------------------------------------|
| USERNAME | DB user to login with                                    | liquibase 						            |
| PASSWORD | DB password to login with                                | liquibase  									|
| DATABASE | Database to Login to                                     | postgres                                    |
| HOSTNAME | Hostname that the DB is reachable on                     | postgres                                    |
| PORT     | Post that the DB is reachable on                         | 5432                                        | 
| JDBC_URL | Override to specify a custom JDBC url (Must be Postgres) | jdbc:postgresql://$HOSTNAME:$PORT/$DATABASE |
| ACTION   | Which Liquibase Action to run                            | migrate                                     |

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)