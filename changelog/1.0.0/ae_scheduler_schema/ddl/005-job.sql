create table if not exists ae_scheduler_schema.job
(
    id      bigint not null,
    git_url varchar(255),
    name    varchar(255),
    constraint job_pkey
        primary key (id)
);