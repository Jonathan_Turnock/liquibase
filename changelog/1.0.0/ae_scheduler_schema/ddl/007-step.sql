create table if not exists ae_scheduler_schema.step
(
    id     bigint not null,
    image  varchar(255),
    name   varchar(255),
    number bigint,
    status varchar(255),
    run_id bigint,
    constraint step_pkey
        primary key (id),
    constraint step_job_id_fk
        foreign key (run_id) references ae_scheduler_schema.run
            on delete cascade
);