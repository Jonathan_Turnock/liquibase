create table if not exists ae_scheduler_schema.run
(
    id      bigint not null,
    ended   timestamp,
    started timestamp,
    status  varchar(255),
    job_id  bigint,
    constraint run_pkey
        primary key (id),
    constraint run_job_id_fk
        foreign key (job_id) references ae_scheduler_schema.job
            on delete cascade
);