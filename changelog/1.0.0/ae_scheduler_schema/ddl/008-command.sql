create table if not exists ae_scheduler_schema.command
(
    id          bigint not null,
    instruction varchar(255),
    number      bigint,
    std_err     text,
    std_out     text,
    step_id     bigint,
    constraint command_pkey
        primary key (id),
    constraint command_step_id_fk
        foreign key (step_id) references ae_scheduler_schema.step
            on delete cascade
);